export ZSH="/home/leo/.oh-my-zsh"
ZSH_THEME="gentoo"
plugins=(git)
source $ZSH/oh-my-zsh.sh

# Alias
alias qlop='qlop -H'
