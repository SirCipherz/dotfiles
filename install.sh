#!/usr/bin/bash

cp -r .config ~/
cp -r .emacs.d ~/
cp .vimrc ~/
cp .zshrc ~/
cp .Xresources ~/

echo "Everything is installed"
